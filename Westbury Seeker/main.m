//
//  main.m
//  Westbury Seeker
//
//  Created by Tuan on 4/7/15.
//  Copyright (c) 2015 Tuan Amith. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
