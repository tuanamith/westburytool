//
//  ViewController.m
//  Westbury Seeker
//
//  Created by Tuan on 4/7/15.
//  Copyright (c) 2015 Tuan Amith. All rights reserved.
//

#import "ViewController.h"
#import "DDFileReader.h"

@implementation ViewController

@synthesize statusString =_statusString;
@synthesize txtDocDelimiter = _txtDocDelimiter;
@synthesize urlString;
@synthesize documentString;
@synthesize searchSpace;
@synthesize searchTerms;

- (IBAction)onOpenWestburyFile:(id)sender {
    
    NSLog(@"Opening file");
    NSOpenPanel *openDialog  =[NSOpenPanel openPanel];
    [openDialog setCanChooseFiles:YES];
    
    NSInteger result = [openDialog runModal];
    if(result == NSFileHandlingPanelOKButton){
        
        urlString = [[NSURL alloc]initFileURLWithPath:[openDialog URL].path];
        
        
        
    }
    
    if (urlString){
        [[self statusString]setStringValue:[urlString path]];
        post_count = 0;
        
        //NSLog(@"Stuff in search space: %@", [searchSpace string]);
    }
    
    
}

-(void)processFile:(NSURL *)urlArgument{
    //NSLog(@"%@", [urlArgument path]);
    
    //Uncomment if using the old technique
    /*uint64 offset = 0;
     uint32 chunkSize = 2048;
     NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:[urlArgument path]];
     NSData *data = [handle readDataOfLength:chunkSize];
     
     
     NSMutableString* compiledFile = [NSMutableString stringWithCapacity:1];
     NSString* docDelimiter = self.txtDocDelimiter.stringValue;*/
    
    
    
    
    
    /*DDFileReader * reader = [[DDFileReader alloc] initWithFilePath:[urlArgument path]];
     NSString * line = nil;
     self.documentString = [[NSMutableString alloc]init];
     while ((line = [reader readLine])) {
     
     
     
     if ([line length]>0) {
     if([line rangeOfString:docDelimiter].location != NSNotFound){
     NSLog(@"*****END OF DOCUMENT FOUND*****");
     [self saveFile:self.documentString];
     
     
     [self.documentString setString:@""];
     }
     else{
     
     
     [self.documentString appendString:line];
     
     }
     }
     
     
     }
     [reader release];*/
    @autoreleasepool {
        searchTerms = [[NSMutableSet alloc]init];
        
        [searchTerms addObjectsFromArray:[[searchSpace string]componentsSeparatedByString:@"\n"]];
        
        
        NSString* docDelimiter = self.txtDocDelimiter.stringValue;
        
        
        documentString = [[NSMutableString alloc]initWithCapacity:0];
        
        DDFileReader * reader = [[DDFileReader alloc] initWithFilePath:[urlArgument path]];
        post_count = 0;
        [reader enumerateLinesUsingBlock:^(NSString * line, BOOL * stop) {
            post_count++;
            if ([line length]>0) {
                if([line rangeOfString:docDelimiter].location != NSNotFound){
                    //NSLog(@"*****END OF DOCUMENT FOUND*****");
                    /*if([documentString rangeOfString:@"vaccine"].location !=NSNotFound){
                     [self saveFile:documentString andAddFileName:post_count];
                     }*/
                    for(id searchTerm in searchTerms){
                        @autoreleasepool {
                            if([documentString rangeOfString:searchTerm].location !=NSNotFound){
                                [self saveFile:documentString andAddFileName:post_count];
                            }
                        }
                    }
                    
                    [documentString release];
                    documentString = nil;
                    documentString = [[NSMutableString alloc]initWithCapacity:0];
                    
                    //[documentString setString:@""];
                }
                else{
                    
                    
                    [documentString appendString:line];
                    
                }
            }
            [line release];
            line = nil;
            //NSLog(@"Post count of %llu", post_count);
        }];
        [reader release];
        reader = nil;
        [documentString release];
        documentString = nil;
        
        [searchTerms release];
        searchTerms = nil;
        
        NSAlert* msgBox = [[[NSAlert alloc] init] autorelease];
        [msgBox setMessageText: @"Mission Accomplished (TM)."];
        [msgBox addButtonWithTitle: @"OK"];
        [msgBox runModal];
        
        //removed code and replaced with above
    }
}



-(void)saveFile:(NSString*)compiledData andAddFileName:(unsigned long long)count_of_post{
    NSLog(@"Path is %@", [urlString lastPathComponent]);
    
    //[newFileName release];
    NSString* newFileName = nil;
    
    //[outputDir release];
    NSString *outputDir = nil;
    
    //[fileContents release];
    NSString* fileContents = nil;
    
    //[lastComponent release];
    NSString* lastComponent = nil;
    
    fileContents = compiledData;
    lastComponent = [urlString lastPathComponent];
    
    //create output directory
    BOOL directoryExists;
    NSFileManager* manager = [NSFileManager defaultManager];
    outputDir =  [[[urlString URLByDeletingLastPathComponent]path] stringByAppendingPathComponent:@"output"];
    //NSLog(@"the output directory is %@", outputDir);
    if (![manager fileExistsAtPath:outputDir isDirectory:&directoryExists] || !directoryExists) {
        NSError *error = nil;
        [manager createDirectoryAtPath:outputDir withIntermediateDirectories:YES attributes:nil error:&error];
        if (error)
            NSLog(@"Error creating directory path: %@", [error localizedDescription]);
        
    }
    
    newFileName = [[urlString path] stringByReplacingOccurrencesOfString:lastComponent withString:
                   [NSString stringWithFormat:@"output/%llu.txt", post_count]
                   ];
    
    //NSLog(@"New path is %@", newFileName);
    //NSLog(@"new file path %@", [[urlString path]stringByReplacingOccurrencesOfString:lastComponent withString:self.createRandomFileName]);
    
    NSError *error = nil;
    [fileContents writeToFile:newFileName atomically:NO encoding:NSUTF8StringEncoding error:&error];
    if (error)
        NSLog(@"Error creating file: %@", [error localizedDescription]);
    
    
    //[newFileName release];
    newFileName = nil;
    
    //[outputDir release];
    outputDir = nil;
    
    //[fileContents release];
    fileContents = nil;
    
    //[lastComponent release];
    lastComponent = nil;
    
    //compiledData = nil;
    
    //[NSString stringWithFormat:@"output/%@", [self createRandomFileName]];
    
    
}

-(NSString*)createRandomFileName{
    //produce random number
    unsigned int random_digits = 1+ arc4random()%1000000000;
    NSLog(@"%d", random_digits);
    
    //get time and date
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd-hhmmss"];
    NSString * dateTime = [DateFormatter stringFromDate:[NSDate date]];
    
    NSString* fileName = [[NSString stringWithFormat:@"%d-%@.txt", random_digits, dateTime]autorelease];
    [DateFormatter release];
    DateFormatter = nil;
    return fileName;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
- (void)dealloc
{
    if (urlString) {
        [urlString release];
    }
    [super dealloc];
}
- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    
    // Update the view, if already loaded.
}

- (IBAction)breakUpFile:(id)sender {
    if(urlString){
        [self processFile:urlString];
    }
}
@end
