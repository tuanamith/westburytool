//
//  ViewController.h
//  Westbury Seeker
//
//  Created by Tuan on 4/7/15.
//  Copyright (c) 2015 Tuan Amith. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController{
    NSURL* urlString;
    unsigned long long post_count;
    NSMutableSet* searchTerms;
}
@property (assign) IBOutlet NSTextView *searchSpace;

-(void)processFile:(NSURL*)urlArgument;
@property (retain) NSURL* urlString;
@property (nonatomic, copy) NSMutableString* documentString;
@property (nonatomic, retain) NSMutableSet* searchTerms;
@property (retain) IBOutlet NSTextField *statusString;

- (IBAction)breakUpFile:(id)sender;
@property (retain) IBOutlet NSTextField *txtDocDelimiter;


@end

